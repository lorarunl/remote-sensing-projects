## Ce programme effectue l'évolution de la surface de neige en prennant comme
## données des images dans un dossier se trouvant dans le même répertoire 
## que le fichier .py.
## Pour ce programme, les images doivent s'être vues appliquées un filtre 
## colorant en cyan la neige et laissant en blanc les nuages.
## Contrairement à la version v1, ici les photos doivent être au format aammjj.jpg


# -*- coding: utf-8 -*-
import os
from PIL import Image
import matplotlib.pyplot as plt 
import numpy as np 
import sys


### Entrée des données : échelle, dossier à analyser, nom du fichier avec les résultats ###
### Ainsi que la création de la liste avec les fichiers à analyser ###

liste=[] # liste des surfaces de neige / axe vertical
dates = [] # liste des dates auxquelles ont été prises les images / axe horizontal
photos = []
m=0
print("\nEntrez la valeur de l'échelle (longueur en mètre d'un pixel)")
scale = int(input()) # l'utilisateur entre la taille du côté d'un pixel, pour toutes les images
liste_pix_neige = []
liste_pix_nuages = []
pcneige = []
liste_pc_nuages = [] # Contient les pourcentage de neige pour chaque photo

# Choix du dossier à analyser
print("\nQuel dossier voulez-vous analyser ?")
dossier = input() # Les photos doivent être au format aammjj
if dossier not in os.listdir():
    while dossier not in os.listdir():
        print("\nCe dossier n'existe pas.")
        print("\nQuel dossier voulez-vous analyser ?")
        dossier = input()
os.chdir(str(dossier))

# Sélection de toutes les photos avec un nom au bon format aammjj

for photo in os.listdir(): # Selection des photos dans le dossier
    if photo.endswith('.jpg') and len(photo) == 10: # On vérifie le bon format des images et le bon nombre de caractère 
        photos.append(photo) # Si elles sont du bon format et bien nommées, on les met dans la liste
if len(photos) == 0:
    print ( "\n Erreur : PAS DE PHOTO AU BON FORMAT")
    sys.exit()
    
# Choix du nom avec test si le nom existe déja    
    
print("\nComment voulez-vous nommer le fichier avec les données ?") # Choix du nom du fichier resultat
nom = input()
if nom+".txt" in os.listdir() :
    while nom+".txt" in os.listdir(): # Boucle pour le choix du nom
        print ("\nCe fichier existe déjà, voulez-vous l'écraser?\n\nOui : 1  Non : 0")
        m = int(input())
        if m == 1:
            os.remove(str(nom)+".txt")
        if m == 0:
            print("\nComment voulez-vous nommer le fichier avec les données ?")
            nom = input()
        
    
for i in range(len(photos)): # On trie le dossier par ordre croissant (pour le graphe a la fin)
       mini = i # On veut mettre le minimum au rang i
       for j in range(i+1, len(photos)): # On cherche le rang du minimum 
           if photos[mini] > photos[j]:
               mini = j
       va = photos[i] # On stocke la valeur du rang dans une variable auxiliaire pour ne pas l'ecraser
       photos[i] = photos[mini]
       photos[mini] = va
if len(photos) != 0:    
       print("\nLes photos analysees sont :",photos,"\n")


### Traitement des images ###

for photo in photos : # On attaque le traitement de tout le dossier
    print ("Analyse de",photo,"...\n")
    img = Image.open(photo) # img est l'image qu'on peut utiliser ici
    
# axe temporel
    date = 100000*int(photo[0])+10000*int(photo[1])+1000*int(photo[2])+100*int(photo[3])+10*int(photo[4])+int(photo[5])
    # on transforme la chaine de caractères en entier avec int
    dates.append(date) # on ajoute cette date à la liste de dates

# pixels
    taillex = img.size[0] # nombre de pixels sur une ligne (horizontal)
    tailley = img.size[1] # nbr de pixels sur une colonne (vertical)

# initialisations
    L = [] # liste des coordonnées des pixels blancs
    r,v,b = 0,0,0 # valeurs de Rouge, Vert, Bleu
    nb_pix_neige = 0 # nombre de pixels blancs
    nb_pix_nuages = 0

# boucle : balayage de tous les pixels
    limite = 235 # définir la valeur des composantes à partir de laquelle le pixel est considéré blanc
    # avec la valeur 235, le gris clair n'est pas compté (essai effectué) : valeur cohérente
    for i in range(taillex): # on balaie l'horizontale
        for j in range (tailley): # on balaie ensuite la verticale : on procède par colonnes
            r,v,b=img.getpixel((i,j))[0],img.getpixel((i,j))[1],img.getpixel((i,j))[2] # r,v,b prennent les valeurs des 3 composantes du pixel (i,j)
            if r > limite and v > limite and b > limite: # si les 3 composantes sont claires
            # ces conditions fonctionnent : nb_pix_blancs varie de manière cohérente si on modifie la limite
                nb_pix_nuages += 1 # il y a un pixel blanc en plus
            if r < 80 and v > limite and b > limite:
                nb_pix_neige += 1
                L.append([i,j]) # et le couple de coordonnées de ce pixel est enregistré dans la liste
                
    liste_pix_neige.append(nb_pix_neige)
    liste_pix_nuages.append(nb_pix_nuages)  
    liste_pc_nuages.append(nb_pix_nuages/(taillex*tailley))


### Ecriture des résultats ###

# Les résultats sont écrits dans un fichier texte dans le même dossier que les photos
    
for photo in photos:
    fichier = open(str(nom)+".txt","a") # on ouvre le fichier texte que'on a préalablement nommé pour le compléter
    
    fichier.write("\n//\t") # saute une ligne
    fichier.write(str(photo)) # on écrit le nom de l'image
    fichier.write("\t//\n\nNombre de pixels de neige = ")
    fichier.write(str(liste_pix_neige[photos.index(photo)])) # on doit écrire une chaine de caractères : on transforme
    fichier.write("\nSurface totale de neige = ")
    fichier.write(str(liste_pix_neige[photos.index(photo)]*(scale**2)))
    fichier.write(" m²")
    fichier.write("\n\n") 
    fichier.close() # on ferme le fichier texte
    # affichages directement dans le terminal
    print("//",photo,"//\n\nNombre de pixels de neige = ",liste_pix_neige[photos.index(photo)])
    print("La surface totale de neige est de ",liste_pix_neige[photos.index(photo)]*(scale**2)," m²")
    print("Le pourcentage de nuages sur cette photo est de ",round(100*liste_pc_nuages[photos.index(photo)],4),"%\n")
    

    
    
# ajout de la surface neigeuse dans la liste
    liste.append(int(liste_pix_neige[photos.index(photo)]*(scale**2)))
    # on ajoute cette valeur au même indice dans la liste que la date indiquée plus haut


### Tracé ###
    
plt.figure()
plt.plot(dates,liste) # surface de neige en fonction de la date
plt.title("Evolution de la surface de neige au cours du temps")
plt.xlabel("Date en aa/mm/jj")
plt.ylabel("Surface en m²")
plt.show()
